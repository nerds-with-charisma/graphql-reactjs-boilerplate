const React = require("react")
const { AuthProvider } = require('./src/context/auth');

exports.wrapRootElement = ({ element }) => {
  return (
    <AuthProvider>
      {element}
    </AuthProvider>
  )
}
const path = require('path');

// Implement the Gatsby API “createPages”. This is called once the
// data layer is bootstrapped to let plugins create pages from data.
exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  // Query for markdown nodes to use in creating pages.
  const result = await graphql(
    `
      {
        nwc {
          getPosts {
            body
            commentCount
            createdAt
            id
            likeCount
            username
            likes {
              id
              username
              createdAt
            }
            comments {
              id
              body
              createdAt
              username
            }
          }
        }
      }
    `
  );

  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  // Create pages for each markdown file.
  const blogPostTemplate = path.resolve(`src/pages/posts.js`);

  result.data.nwc.getPosts.forEach((props) => {
    // console.log(`🔥 ${JSON.stringify(props)}`);
    const path =  `/posts/${props.id}`;
    createPage({
      path,
      component: blogPostTemplate,
      // In your blog post template's graphql query, you can use path
      // as a GraphQL variable to query for data from the markdown file.
      context: {
        path,
        ...props,
      },
    })
  })
}
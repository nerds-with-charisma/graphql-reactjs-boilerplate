import React from 'react';

import Layout from '../components/layout';

import "@nerds-with-charisma/nerds-style-sass/main.scss";

import Home from '../components/home';

const IndexPage = () => (
  <Layout>
    <Home />
  </Layout>
);

export default IndexPage

import React, { useContext } from 'react';

import { AuthContext } from '../context/auth';

import Layout from '../components/layout';
import PostSingle from '../components/postSingle';

const Posts = ({ pageContext }) => {
  const { user } = useContext(AuthContext);

  return (
    <Layout>
      <PostSingle user={user} pageContext={pageContext} />
    </Layout>
  )
};

export default Posts;

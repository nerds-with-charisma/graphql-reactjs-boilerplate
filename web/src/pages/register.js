import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';

import Register from '../components/register';

const RegisterWrapper = () =>  (
  <Layout>
    <SEO title="Register" />

    <Register />
  </Layout>
);


export default RegisterWrapper;

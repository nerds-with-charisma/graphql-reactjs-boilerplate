import React, { useContext, useState } from 'react';
import { Link } from 'gatsby';

import { AuthContext } from '../context/auth';

const Header = () => {
  const { user, logout } = useContext(AuthContext);

    return (
    <header>
      <nav className="grid padding--md font--14">
        <div className="col-6">
          <Link to="/" className="padding--md font--text" activeClassName="font--primary border--bottom">
            {'Home'}
          </Link>
        </div>
        { (!user) ? (
          <div className="col-6 text--right">
            <Link to="/login" className="padding--md font--text" activeClassName="font--primary border--bottom">
              {'Login'}
            </Link>

            <Link to="/register" className="padding--md font--text" activeClassName="font--primary border--bottom">
              {'Register'}
            </Link>
          </div>
        ) : (
          <div className="col-6 text--right">
            {`Hi, ${user.username} `}
            &nbsp;
            &nbsp;
            &nbsp;
            <button
              type="button"
              className="btn default radius--lg"
              onClick={() => logout()}
            >
              {'Logout'}
            </button>
          </div>
        )
        }
      </nav>
      <br />
    </header>
  )
};

export default Header;

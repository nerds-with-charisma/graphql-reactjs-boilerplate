import React, { useContext, useState } from 'react';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/react-hooks';
import { navigate } from 'gatsby';

import { Input } from '@nerds-with-charisma/nerds-style-sass';


import { AuthContext } from '../context/auth';

const Login = () => {
  const context = useContext(AuthContext);
  const [errors, errorsSetter] = useState({});
  const [values, valuesSetter] = useState({
    username: '',
    password: '',
  });

  // if already logged in, send them home
  if (context.user && context.user.id) navigate('/');

  const [loginUser, { loading }] = useMutation(LOGIN_USER, {
    update(proxy, result){
      context.login(result.data.login);
      navigate('/');
    },
    onError(e) {
      errorsSetter(e.graphQLErrors[0].extensions.exception.errors);
    },
    variables: values,
  });

  const onSubmit = (e) => {
    e.preventDefault();
    loginUser();
  };

  return (
    <form onSubmit={(e) => onSubmit(e)} noValidate className="container--sm">
      { (loading === true) && (
        <h1>Loading...</h1>
      )}
      <h1>
        {'Login'}
      </h1>

      { Object.keys(errors).length > 0 && Object.values(errors).map(value => (
        <div className="bg--cta font--light font--12 padding--sm margin--bottom" key={value}>
          {value}
        </div>
      ))}

      <Input
        title="Username"
        id="input--username"
        callback={(v) => valuesSetter({...values, username: v })}
        type="text"
        placeHolder=""
        prepopulatedValue={values.username}
        error={errors.username}
        required
      />

      <Input
        title="Password"
        id="input--password"
        callback={(v) => valuesSetter({...values, password: v })}
        type="password"
        placeHolder=""
        error={errors.password}
        prepopulatedValue={values.password}
        required
      />
      <br />
      <br />

      <button
        type="submit"
        className="btn primary float--right font--14 radius--lg"
      >
        {'Login'}
      </button>
    </form>
  )
};

const LOGIN_USER = gql`
  mutation login (
    $username: String!
    $password: String!
  ) {
    login (
      username: $username,
      password: $password
    ) {
      id
      email
      username
      createdAt
      token
    }
  }
`;

export default Login;

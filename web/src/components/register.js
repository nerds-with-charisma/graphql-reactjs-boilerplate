import React, { useContext, useState } from 'react';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/react-hooks';
import { navigate } from 'gatsby';

import { Input } from '@nerds-with-charisma/nerds-style-sass';

import { AuthContext } from '../context/auth';

const Register = () => {
  const context = useContext(AuthContext);
  const [errors, errorsSetter] = useState({});
  const [values, valuesSetter] = useState({
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
  });

  // if already logged in, send them home
  if (context.user && context.user.id) navigate('/');

  const [addUser, { loading }] = useMutation(REGISTER_USER, {
    update(proxy, result){
      context.login(result.data.login);
      navigate('/');
    },
    onError(e) {
      errorsSetter(e.graphQLErrors[0].extensions.exception.errors);
    },
    variables: values,
  });

  const onSubmit = (e) => {
    e.preventDefault();
    addUser();
  };

  return (
    <form onSubmit={(e) => onSubmit(e)} noValidate className="container--sm">
      { (loading === true) && (
        <h1>Loading...</h1>
      )}
      <h1>
        {'Register'}
      </h1>

      { Object.keys(errors).length > 0 && Object.values(errors).map(value => (
        <div className="bg--cta font--light font--12 padding--sm margin--bottom" key={value}>
          {value}
        </div>
      ))}

      <Input
        title="Username"
        id="input--username"
        callback={(v) => valuesSetter({...values, username: v })}
        type="text"
        placeHolder=""
        prepopulatedValue={values.username}
        error={errors.username}
        required
      />

      <Input
        title="Email"
        id="input--email"
        callback={(v) => valuesSetter({...values, email: v })}
        type="email"
        placeHolder=""
        prepopulatedValue={values.email}
        error={errors.email}
        required
      />

      <Input
        title="Password"
        id="input--password"
        callback={(v) => valuesSetter({...values, password: v })}
        type="password"
        placeHolder=""
        error={errors.password}
        prepopulatedValue={values.password}
        required
      />

      <Input
        title="Confirm Password"
        id="input--confirmPassword"
        callback={(v) => valuesSetter({...values, confirmPassword: v })}
        type="password"
        placeHolder=""
        prepopulatedValue={values.confirmPassword}
        error={errors.confirmPassword}
        required
      />
      <br />
      <br />

      <button
        type="submit"
        className="btn primary float--right font--14 radius--lg"
      >
        {'Register'}
      </button>
    </form>
  )
};

const REGISTER_USER = gql`
  mutation register (
    $username: String!
    $email: String!
    $password: String!
    $confirmPassword: String!
  ) {
    register (
      registerInput: {
        username: $username,
        email: $email
        password: $password
        confirmPassword: $confirmPassword
      }
    ) {
      id
      email
      username
      createdAt
      token
    }
  }
`;

export default Register;

import React from 'react';
import { navigate } from 'gatsby';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/react-hooks';

import { FETCH_POSTS_QUERY } from '../util/graphql';

const DeleteButton = ({ postId, commentId }) => {
  const mutation = (commentId) ? DELETE_COMMENT_MUTATION : DELETE_POST_MUTATION;

  const [deletePostOrMutation] = useMutation(mutation, {
    update(proxy, result) {
      if (!commentId) {
        console.log('not a comment');
        const data = proxy.readQuery({
          query: FETCH_POSTS_QUERY,
        });

        data.getPosts = data.getPosts.filter(p => p.id !== postId);
        proxy.writeQuery({ query: FETCH_POSTS_QUERY, data });
        navigate('/');
      };
    },
    variables: {
      postId,
      commentId,
    }
  });

  return (
    <button
      type="button"
      className="btn font--cta border--cta radius--lg float--right"
      onClick={() => {
        const sureYouWantToDelete = window.confirm('Are you sure you want to delete this post');
        if (sureYouWantToDelete === true) deletePostOrMutation();
      }}
    >
      Delete
    </button>
  )
};

const DELETE_POST_MUTATION = gql`
  mutation deletePost($postId: ID!) {
    deletePost(postId: $postId)
  }
`;

const DELETE_COMMENT_MUTATION = gql`
  mutation deleteComment($commentId: ID!, $postId: ID!) {
    deleteComment(commentId: $commentId, postId: $postId) {
      id
      comments {
        id createdAt body
      }
      commentCount
    }
  }
`;

const FETCH_POST_QUERY = gql`
  query($postId: ID!) {
    getPost(postId: $postId) {
      body
      commentCount
      createdAt
      id
      likeCount
      username
      likes {
        id
        username
        createdAt
      }
    }
  }
`;


export default DeleteButton;

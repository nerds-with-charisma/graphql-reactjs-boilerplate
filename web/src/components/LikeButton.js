import React, { useEffect, useState } from 'react';
import { navigate } from 'gatsby';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';

const LikeButton = ({ post, user }) => {
  let likes = post.likes;
  const [liked, likedSetter] = useState(false);

  useEffect(() => {
    if (post && user && likes.find(like => like.username === user.username)) {
      // if any of the likes came from this user
      likedSetter(true);
    } else {
      likedSetter(false);
    }
  }, [user, likes, post]);

  const [likePost] = useMutation(LIKE_POST_MUTATION, {
    variables: { postId: post.id },
    update: (proxy, { data }) => {
      likedSetter(!liked);
    },
    optimisticResponse: {
      __typename: "Mutation",
      likePost: {
        ...post,
        likeCount: (!liked) ? post.likeCount + 1 : post.likeCount - 1,
      },
    }
  });

  const likeButton = user ? (
    liked ? (
      <button
        type="button"
        className="btn font--light bg--info radius--lg"
        onClick={() => likePost()}
      >
        &hearts;
        {' - '}
        {post.likeCount}
      </button>
    ) : (
      <button
        type="button"
        className="btn font--info border--info radius--lg"
        onClick={() => likePost()}
      >
        &hearts;
        {' - '}
        {post.likeCount}
      </button>
    )
  ) : (
    <button
      type="button"
      className="btn font--info border--info radius--lg"
      onClick={() => navigate('/login')}
    >
      &hearts;
      {' - '}
      {post.likeCount}
    </button>
  );

  return (
    <span>
      {likeButton}
    </span>
  )
};

const LIKE_POST_MUTATION = gql`
  mutation likePost($postId: ID!) {
    likePost(postId: $postId) {
      id
      likes {
        id
        username
      }
      likeCount
    }
  }
`;

export default LikeButton;

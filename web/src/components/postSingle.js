import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { useQuery, useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';

import { Input } from '@nerds-with-charisma/nerds-style-sass';

import LikeButton from '../components/LikeButton';
import DeleteButton from '../components/DeleteButton';


const PostSingle = ({ pageContext, user }) => {
  const [post, postSetter] = useState({});

  const { data } = useQuery(FETCH_POST_QUERY, {
    update(proxy, result) {
      const data = proxy.readQuery({
        query: FETCH_POST_QUERY,
      });
      console.log(data);

      postSetter(data);
    },
    variables: {
      postId: pageContext.id,
    },
  });

  const [comment, commentSetter] = useState('');
  const [submitComment] = useMutation(SUBMIT_COMMENT_MUTATION, {
    update() {
      commentSetter('');
    },
    variables: {
      postId: pageContext.id,
      body: comment,
    }
  });

  useEffect(() => {
    postSetter(pageContext);  // loaded from ssr
  }, []);

  // once data is fetched from server
  useEffect(() => {
    if (data) postSetter(data.getPost); // not loaded from ssr
  }, [data])


  return (
    <div className="container--md grid">
      <div className="col-12">
        <img
          src="https://nerdswithcharisma.com/portfolio/maeby.jpg"
          className="float--right radius--lg"
          style={{ height: 40 }}
          alt="avatar"
        />

        <div className="padding--sm font--12">
          {post.username}
          <br />
          <span className="font--10 font--grey">
            {`${moment(post.createdAt).fromNow(true)} ago`}
          </span>
        </div>
        <div className="font--21 padding--sm border--bottom">
          {post.body}
        </div>

        <div className="font--12 padding--sm">
          <LikeButton
            post={pageContext}
            user={user}
          />
          &nbsp;&nbsp;
          <button
            type="button"
            className="btn font--cta border--cta radius--lg"
            onClick={() => console.log('comment')}
          >
            &#x1f5ea;
            {' - '}
            {post.commentCount}
          </button>
          &nbsp;&nbsp;
          { user && (user.username === post.username) && (
            <DeleteButton
              postId={post.id}
            />
          )}
        </div>
      </div>

      <section id="comments--wrapper" className="col-12 font--12">
        <br />
        { post && post.comments && post.comments.map(comment => (
          <div className="col-12 border margin--sm padding--md radius--md" key={comment.id}>
            <strong>
              {comment.username || 'Anonymous'}
            </strong>
            <br />
            {`${moment(comment.createdAt).fromNow(true)} ago`}
            <hr />
            {comment.body}
            <br />
            <br />
            { user && (user.username === comment.username) && (
              <>
                <DeleteButton
                  postId={post.id}
                  commentId={comment.id}
                />
                <br />
                <br />
              </>
            )}
          </div>)
        )}
      </section>

      <br />

      { user && (
        <section id="new--comment" className="col-12">
          <strong>Add Comment</strong>
          <br />
          <Input
            title="Comment Body"
            id="input--comment"
            callback={(v) => commentSetter(v)}
            type="text"
            placeHolder=""
          />
          <br />
          <br />
          <button
            type="button"
            className="float--right btn default radius--lg"
            onClick={() => submitComment()}
            disabled={comment === ''}
          >
            {'Add comment'}
          </button>
        </section>
      )}
    </div>
  )
};

const FETCH_POST_QUERY = gql`
  query($postId: ID!) {
    getPost(postId: $postId) {
      body
      commentCount
      createdAt
      id
      likeCount
      username
      comments {
        id
        body
        createdAt
        username
      }
      likes {
        id
        username
        createdAt
      }
    }
  }
`;

const SUBMIT_COMMENT_MUTATION = gql`
  mutation($postId: ID!, $body: String!) {
    createComment(postId: $postId, body: $body) {
      id
      comments {
        id body createdAt username
      }
      commentCount
    }
  }
`;

export default PostSingle;

import React from 'react';
import PropTypes from 'prop-types';

import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createHttpLink } from 'apollo-link-http';
import { ApolloProvider } from '@apollo/react-hooks';
import { setContext } from 'apollo-link-context';

import Header from './header';

const httpLink = createHttpLink({
  uri: 'http://localhost:5000',
});

const authLink = setContext((req, pre) => {
  const token = localStorage.getItem('jwtToken');
  return {
    headers: {
      Authorization: token ? `Bearer ${token}` : '',
    },
  };
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

const Layout = ({ children }) => {
  return (
    <ApolloProvider client={client}>
      <main className="container--md">
        <Header />

        {children}
      </main>
    </ApolloProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout

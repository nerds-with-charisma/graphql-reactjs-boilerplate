import React, { useContext } from 'react';
import { Link, navigate } from 'gatsby';
import { useQuery } from '@apollo/react-hooks';
import moment from 'moment';

import { FETCH_POSTS_QUERY } from '../util/graphql';
import { AuthContext } from '../context/auth';
import PostForm from '../components/PostForm';

import LikeButton from '../components/LikeButton';
import DeleteButton from '../components/DeleteButton';

const Home = () => {
  const { user } = useContext(AuthContext);

  const { loading, data } = useQuery(FETCH_POSTS_QUERY);

  if (data) console.log(data);

  const commentOnPost = (id) => {
    navigate(`/posts/${id}`);
  };

  return (
    <main id="home" className="grid">
      <h1 className="col-12">
        {'Recent Posts'}
      </h1>

      <div className="col-4">
        <PostForm />
      </div>

      {(loading) ? (
        <strong className="col-12">Loading posts...</strong>
      ) : (
        data && data.getPosts && data.getPosts.map(post => (
          <div key={post.id} className="col-4 border padding--sm radius--sm margin--bottom">
            <img
              src="https://nerdswithcharisma.com/portfolio/maeby.jpg"
              className="float--right radius--lg"
              style={{ height: 40 }}
              alt="avatar"
            />
            <div className="padding--sm font--12">
              {post.username}
              <br />
              <Link to={`/posts/${post.id}`} className="font--10 font--grey">
                {moment(post.createdAt).fromNow(true)}
              </Link>
            </div>

            <div className="font--14 padding--sm border--bottom">
              <Link to={`/posts/${post.id}`}>
                <strong className="font--text">
                  {post.body}
                </strong>
              </Link>
            </div>
            <div className="font--12 padding--sm">
              <LikeButton
                post={post}
                user={user}
              />
              &nbsp;&nbsp;
              <button
                type="button"
                className="btn font--cta border--cta radius--lg"
                onClick={() => commentOnPost(post.id)}
              >
                &#x1f5ea;
                {' - '}
                {post.commentCount}
              </button>
              &nbsp;&nbsp;
              { user && (user.username === post.username) && (
                <DeleteButton
                  postId={post.id}
                />
              )}
            </div>
          </div>
        ))
      )}

      { !loading && !data && (
        <div className="col-12 text--center">
          {'There was an error getting our posts...yeah, it\'s embarressing...we\'ll try better next time.'}
        </div>
      )}
    </main>
  )
};

export default Home;

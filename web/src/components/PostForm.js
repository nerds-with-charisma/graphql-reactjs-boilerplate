import React, { useState } from 'react';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/react-hooks';
import { navigate } from 'gatsby';

import { Input } from '@nerds-with-charisma/nerds-style-sass';

import { FETCH_POSTS_QUERY } from '../util/graphql';

const PostForm = () => {
  const [error, errorSetter] =useState(null);
  const [values, valuesSetter] = useState({});

  const onSubmit = (e) => {
    e.preventDefault();
    createPost();
  };

  const [ createPost, { errors }] = useMutation(CREATE_POST_MUTATION, {
    variables: values,
    update(proxy, result) {
      const data = proxy.readQuery({
        query: FETCH_POSTS_QUERY,
      }); // fetch posts from client data in cache

      data.getPosts = [result.data.createPost, ...data.getPosts];

      proxy.writeQuery({ query: FETCH_POSTS_QUERY, data });

      valuesSetter({});
      navigate('/');  // work around to deal with cache
    },
    onError(error) {
      errorSetter(error.graphQLErrors[0].message);
    }
  });

  return (
    <form onSubmit={(e) => onSubmit(e)}>
      <strong>Create Post</strong>
      <Input
        title="Post Body"
        id="input--postBody"
        callback={(v) => valuesSetter({...values, body: v })}
        type="text"
        error={error}
        placeHolder="Post something"
        prepopulatedValue={values.body}
        required
      />
    </form>
  )
};

const CREATE_POST_MUTATION = gql`
  mutation createPost($body: String!) {
    createPost(body: $body) {
      id
      body
      createdAt
      username
      likes {
        id
        username
        createdAt
      }
      likeCount
      comments {
        id
        body
        username
      }
      commentCount
    }
  }
`;

export default PostForm;

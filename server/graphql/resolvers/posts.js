const { AuthenticationError, UserInputError } = require('apollo-server');

const Post = require('../../models/Post');
const checkAuth = require('../../util/checkAuth');

module.exports = {
  Query: {
    getPosts: async (parent, args, ctx, info) => {
      try {
        const posts = await Post.find().sort({ createdAt: -1 });  // get all posts
        return posts;
      } catch(err) {
        throw new Error(err);
      }

      /* example query
        {
          getPosts{
            id
            body
            username
            createdAt
            commentCount
            comments{
              id
              body
            }
            likeCount
            likes {
              id
              username
              createdAt
            }
          }
        }
      */
    },
    getPost: async (parent, { postId }, ctx, info) => {
      try {
        const post = await Post.findById(postId);  // get specific post
        if (post) {
          return post;
        } else {
          throw new Error('Post not found');
        }
      } catch(err) {
        console.log(err);
        throw new Error(err);
      }

      /* example query
        {
          getPost(postId: "5de973d01c9d440000aeac00") {
            id
            body
            createdAt
            username
          }
        }
      */
    }
  },
  Mutation: {
    createPost: async (parent, { body }, ctx, info) => {
      // make sure user is authenticated before making a post
      const user = checkAuth(ctx);
      console.log(user);

      // no errors, found a user, create the post
      const newPost = new Post({
        body,
        user: user.id,
        username: user.username,
        createdAt: new Date().toISOString(),
      });

      // save it and return to api
      const post = await newPost.save();

      // subscription
      ctx.pubSub.publish('NEW_POST', {
        newPost: post,
      });

      // return the post
      return post;

      /* create post mutation
        mutation {
          createPost(body: "This is another post") {
            id
            body
            createdAt
            username
          }
        }

        Need bearer example:
        { "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkZWE4ODA3ZWVjODQ4OTg5OGJhNTQ3MyIsImVtYWlsIjoidXNlckBlbWFpbC5jb20iLCJ1c2VybmFtZSI6InVzZXIiLCJpYXQiOjE1NzU3MzMyODcsImV4cCI6MTU3NTczNjg4N30.2nMEHASi_DLNPskgr45xYjy4ezZ24G1FsWbQn4j4_Gs" }
      */
    },
    deletePost: async (parent, { postId }, ctx, info) => {
      // get the user
      const user = checkAuth(ctx);

      console.log(user);

      try {
        // get the post
        const post = await Post.findById(postId);
        console.log(post);

        // make sure the post is theirs
        if (user.username === post.username) {
          // delete it
          await post.delete();
          return 'Deleted successfullly';
        } else {
          throw new AuthenticationError('Action not allowed');
        }
      } catch(err) {
        throw new Error(err);
      }

      /*  delete post mutation
        mutation {
          deletePost(postId: "5debc9555eb00cbdd92b024e")
        }
      */
    },
    likePost: async (parent, { postId }, ctx, info) => {
      const user = checkAuth(ctx);

      const post = await Post.findById(postId);
      if (post) {
        // make sure like exists
        if (post.likes.find(like => like.username === user.username)) {
          // we've already liked it, so unlike it
          post.likes = post.likes.filter(like => like.username !== user.username);
        } else {
          // not liked, so like it
          post.likes.push({
            username: user.username,
            createdAt: new Date().toISOString(),
          });
        }

        await post.save();
        return post;
      } else {
        throw new UserInputError('Post not found');
      };

      /* toggle like mutation
        mutation {
          likePost(postId: "5debc9555eb00cbdd92b024e") {
            id
            likes {
              id
              createdAt
              username
            }
            body
          }
        }
      */
    },
  },
  Subscription: {
    newPost: {
      subscribe: (_, __, { pubSub }) => pubSub.asyncIterator('NEW_POST'),
    },
  },
}
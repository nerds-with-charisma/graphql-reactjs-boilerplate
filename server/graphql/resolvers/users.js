const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { SECRET_KEY } = require('../../config');
const User = require('../../models/User');
const { UserInputError } = require('apollo-server');
const { validateRegisterInput, validateLoginInput } = require('../../util/validators');

function generateToken(res) {
  return jwt.sign({
    id: res.id,
    email: res.email,
    username: res.username,

  }, SECRET_KEY, { expiresIn: '1h' });
}

module.exports = {
  Mutation: {
    async login(parent, { username, password }, ctx, info) {
      // validate user data (make sure pw matches, etc)
      const { valid, errors } = validateLoginInput(username, password);

      if (!valid) throw new UserInputError('Errors', { errors });

      // get user from the db
      const user = await User.findOne({ username });
      if (!user) {
        // user not found
        errors.general = 'User not found';
        throw new UserInputError('User not found', { errors });
      } else {
        // compare found user to creds supplied
        const match = await bcrypt.compare(password, user.password);
        if (!match) {
          // bad login
          errors.general = 'Wrong creds';
          throw new UserInputError('Wrong creds', { errors });
        }

        // all good, create the token
        const token = generateToken(user);

        // return the user in the api
        return {
          ...user._doc,
          id: user._id,
          token,
        };
      }

      /* example login mutation
        mutation {
          login(username: "user", password: "123456") {
            id
            email
            token
            username
            createdAt
          }
        }
      */
    },
    async register(parent, { registerInput: { username, email, password, confirmPassword } }, ctx, info) {
      // validate user data (make sure pw matches, etc)
      const { valid, errors } = validateRegisterInput(username, email, password, confirmPassword);
      if (!valid) throw new UserInputError('Errors', { errors });

      // make sure user doesn't already exist
      const user = await User.findOne({ username });
      if (user) {
        throw new UserInputError('Username is taken', {
          errors: {
            username: 'This username is taken',
          },
        });
      }

      // hash the password before storing & create auth token
      password = await bcrypt.hash(password, 12);

      // create new user object
      const newUser = new User({
        email,
        username,
        password,
        createdAt: new Date().toISOString(),
      });

      // save to db
      const res = await newUser.save();

      // create the token
      const token = generateToken(res);

      // return the user in the api
      return {
        ...res._doc,
        id: res._id,
        token,
      };

      /* example mutation
        mutation {
          register(registerInput:{
            username: "user"
            password: "123456"
            confirmPassword: "123456"
            email:"user@email.com"
          }) {
            id
            email
            token
            createdAt
            username
          }
        }
      */
    }
  },
};

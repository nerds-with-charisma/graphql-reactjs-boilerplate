const { AuthenticationError, UserInputError } = require('apollo-server');

const Post = require('../../models/Post');
const checkAuth = require('../../util/checkAuth');

module.exports = {
  Mutation: {
    createComment: async (parent, { postId, body }, ctx, info) => {
      const user = checkAuth(ctx);
      if (body.trim() === '') {
        throw new UserInputError('Empty Comment', {
          errros: {
            body: 'Comment body must not be empty',
          },
        })
      }

      const post = await Post.findById(postId);
      if (post) {
        post.comments.unshift({
          body,
          username: user.username,
          createdAt: new Date().toISOString(),
        });

        await post.save();
        return post;
      } else throw new UserInputError('Post not found');

      /* create comment to a post mutation
        mutation {
          createComment(postId: "5debc9555eb00cbdd92b024e", body: "new comment on a post") {
            id
            body
            comments{
              id
              body
              createdAt
            }
            createdAt
          }
        }
      */
    },
    deleteComment: async (parent, { postId, commentId }, ctx, info) => {
      const user = checkAuth(ctx);
      const post = await Post.findById(postId);
      //if (post.body) { // if there's a post
        // find the index of the desired comment
        const commentIndex = post.comments.findIndex(c => c.id === commentId);
        // make sure the user matches
        if (post.comments[commentIndex].username === user.username) {
          post.comments.splice(commentIndex, 1);
          await post.save();
          return post;
        } else {
          // if it's not their comment, throw an error
          throw new AuthenticationError('Action not allowed to delete this comment');
        }
      //} else {
        //throw new UserInputError('Post not found');
      //}

      /* delete a specific comment on a specific post mutation
        mutation {
          deleteComment(postId: "5debc9555eb00cbdd92b024e", commentId: "5ded9d7986bd44d270d22869") {
            id
            body
            comments {
              id
            }
          }
        }
      */
    }
  }
}
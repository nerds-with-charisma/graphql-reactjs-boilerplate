const { ApolloServer, PubSub } = require('apollo-server');
const mongoose = require('mongoose');

const { MONGODB } = require('./config.js');

const typeDefs = require('./graphql/typeDefs.js');
const resolvers = require('./graphql/resolvers');

const PORT = process.env.PORT || 5000;

// our subscription
const  pubSub = new PubSub();

// setup server
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({ req, pubSub }),
});

// connect to db
mongoose.connect(MONGODB, {
  useNewUrlParser: true,
}).then(() => {
  console.log('MongoDB Connected');

  // start the server
  return server.listen({ port: PORT })
}).then(res => console.log(`Server running on ${res.url}`))
.catch(err => {
  console.error(err);
});
const { AuthenticationError } = require('apollo-server');
const jwt = require('jsonwebtoken');

const { SECRET_KEY } = require('../config');

module.exports = (context) => {
  // console.log(context.req.headers);
  // context had the header for authorization
  const authHeader = context.req.headers.authorization;
  if (authHeader) {
    //if we have an authheader, get the token
    const token = authHeader.split('Bearer ')[1];
    if (token) {
      // verify that the token found is valid
      try {
        const user = jwt.verify(token, SECRET_KEY);
        return user;
      } catch(err) {
        throw new AuthenticationError('Invalid/Expired token');
      }
    } else {
      throw new Error('Authentication token must be \'Bearer [token]\'');
    }
  }

  throw new Error('Auth header must be provided');
}